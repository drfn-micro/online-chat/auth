module gitlab.com/drfn-micro/online-chat/auth

go 1.21.4

require (
	github.com/brianvoe/gofakeit v3.18.0+incompatible // indirect
	github.com/golang/protobuf v1.5.4 // indirect
	github.com/google/wire v0.6.0 // indirect
	golang.org/x/net v0.22.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20240318140521-94a12d6c2237 // indirect
	google.golang.org/grpc v1.64.0 // indirect
	google.golang.org/protobuf v1.33.0 // indirect
)
