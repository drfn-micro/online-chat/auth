//go:build wireinject
// +build wireinject

package factory

import (
	"context"

	"github.com/google/wire"
)

type Service struct{}

func InitializeService(ctx context.Context) (Service, func(), error) {
	panic(wire.Build(provideService))
}

func provideService(
	ctx context.Context,
) Service {
	return Service{}
}
