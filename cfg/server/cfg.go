package servcfg

import "fmt"

// Config is base db config
type Config struct {
	Network string
	Host    string
	Port    string
}

func (c Config) Addr() string {
	return fmt.Sprintf("%s:%s", c.Host, c.Port)
}
