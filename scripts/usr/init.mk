init:
	go mod download
	go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.34.2
	go install github.com/google/wire/cmd/wire@v0.6.0 
	go install golang.org/x/tools/cmd/goimports@v0.22.0

.PHONY: init
