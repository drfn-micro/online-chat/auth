GOLANGCI_LINT_VERSION=v1.56.2

install-golangci-lint:
	GOBIN=${LOCAL_BIN} go install github.com/golangci/golangci-lint/cmd/golangci-lint@$(GOLANGCI_LINT_VERSION)

lint:
	GOBIN=${LOCAL_BIN} ${LOCAL_BIN}/golangci-lint run ./... --config .golangci.pipeline.yaml