fmt:
	@go fmt ./...
	@find . ! -name '*.pb.go' ! -name '*_gen.go' -name \*.go -exec goimports -w {} \;

tidy:
	@go mod tidy

.PHONY: fmt tidy