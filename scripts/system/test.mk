test:
	go test -v ./...

test-race:
	go test -v -race ./...

.PHONY: test test-race