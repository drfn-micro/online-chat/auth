package userserver

import (
	"context"
	"crypto/rand"
	"fmt"
	"io"
	"math/big"
	"net"
	"time"

	"github.com/brianvoe/gofakeit"
	servcfg "gitlab.com/drfn-micro/online-chat/auth/cfg/server"
	userv1 "gitlab.com/drfn-micro/online-chat/auth/pkg/auth/api/user/v1"
	"gitlab.com/drfn-micro/online-chat/auth/pkg/logger"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
)

type userServer struct {
	lg  logger.Logger
	srv *grpc.Server
	Config
	userv1.UnimplementedUserServiceServer
}

type Config struct {
	servcfg.Config
}

func New(cfg Config, lg logger.Logger) *userServer {
	return &userServer{
		lg:     lg,
		Config: cfg,
	}
}

func (us *userServer) Get(_ context.Context, req *userv1.GetRequest) (*userv1.GetResponse, error) {
	return &userv1.GetResponse{
		Id:   req.GetId(),
		Name: gofakeit.Name(),
	}, nil
}

func (us *userServer) Create(context.Context, *userv1.CreateRequest) (*userv1.CreateResponse, error) {
	var r io.Reader
	id, err := rand.Int(r, big.NewInt(big.MaxExp))
	if err != nil {
		return nil, fmt.Errorf("failed to create id")
	}
	return &userv1.CreateResponse{Id: id.Int64()}, nil
}

func (us *userServer) Update(context.Context, *userv1.UpdateRequest) (*emptypb.Empty, error) {
	return &emptypb.Empty{}, nil
}

func (us *userServer) Delete(context.Context, *userv1.DeleteRequest) (*emptypb.Empty, error) {
	return &emptypb.Empty{}, nil
}

func (us *userServer) Run() error {
	us.srv = grpc.NewServer()
	userv1.RegisterUserServiceServer(us.srv, us)

	lis, err := net.Listen(us.Network, us.Addr())
	if err != nil {
		return fmt.Errorf("failed to init listener: %w", err)
	}

	errCh := make(chan error)

	go func() {
		errCh <- us.srv.Serve(lis)
	}()

	select {
	case err := <-errCh:
		return fmt.Errorf("failed to start up grpc server: %w", err)
	case <-time.After(100 * time.Millisecond):
		us.lg = logger.With(us.lg, "addr", us.Addr())
		us.lg.Info("start listening to connections")
		return nil
	}
}

func (us *userServer) ShutDown() {
	us.srv.GracefulStop()
}
