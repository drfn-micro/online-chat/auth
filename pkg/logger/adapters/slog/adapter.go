package slogadapter

import (
	"log/slog"
	"os"

	"gitlab.com/drfn-micro/online-chat/auth/pkg/logger"
)

type slogAdapter struct {
	lg *slog.Logger
}

var _ logger.Logger = (*slogAdapter)(nil)

func NewSlogAdapter(h slog.Handler) *slogAdapter {
	return &slogAdapter{
		lg: slog.New(h),
	}
}

func NewSlogJSONHandler(opts *slog.HandlerOptions) slog.Handler {
	return slog.NewJSONHandler(os.Stdout, opts)
}

func NewAddSourceDebugOptions() *slog.HandlerOptions {
	return &slog.HandlerOptions{
		AddSource: false,
		Level:     slog.LevelDebug,
	}
}

func (l *slogAdapter) Error(err error, args ...any) {
	l.lg.Error(err.Error(), args...)
}

func (l *slogAdapter) Debug(msg string, args ...any) {
	l.lg.Debug(msg, args...)
}

func (l *slogAdapter) Warn(err error, args ...any) {
	l.lg.Warn(err.Error(), args...)
}

func (l *slogAdapter) Info(msg string, args ...any) {
	l.lg.Info(msg, args...)
}

func (l *slogAdapter) With(args ...any) logger.Logger {
	return &slogAdapter{lg: l.lg.With(args...)}
}
