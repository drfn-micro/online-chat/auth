package logger

import (
	"context"
	"sync/atomic"
)

const (
	tIDKey        = "trace_id"
	originNameKey = "origin_name"
)

var defaultLogger atomic.Value

var defaultCtxKey struct{}

// Logger is wrapper type for lfexible dependecy
type Logger interface {
	Error(err error, args ...any)
	Debug(msg string, args ...any)
	Warn(err error, args ...any)
	Info(msg string, args ...any)
}

// Context wraps a Logger value into ctx
func Context(ctx context.Context, lg Logger) context.Context {
	return context.WithValue(ctx, defaultCtxKey, lg)
}

// FromContext unwraps a Logger from context
func FromContext(ctx context.Context, def Logger) Logger {
	if lg, ok := ctx.Value(defaultCtxKey).(Logger); ok {
		return lg
	}
	return def
}

// With adds kv pair to logger structure
func With(lg Logger, k, v string) Logger {
	if lg2, ok := lg.(interface {
		With(a ...any) Logger
	}); ok {
		return lg2.With(k, v)
	}
	return lg
}

// WithTid adds a value with 'trace_id' key to logger structure
func WithTid(lg Logger, tID string) Logger {
	return With(lg, tIDKey, tID)
}

// Default returns default logger instance
func Default() Logger {
	return defaultLogger.Load().(Logger)
}

// SetDefault sets value to default logger instance
func SetDefault(lg Logger) {
	defaultLogger.Store(lg)
}
